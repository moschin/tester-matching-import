# tester-matching-import

The tester-matching-import is used to import of CSV files, parse them and store objects in H2 DB.
This Project contains also backend services for tester-matching-frontend application.

## Getting Started

The chapter will help you install, build and run the tester-matching-import on your local machine. 

### Prerequisites

* Open JDK 11
* Apache Maven 3.x

### Installing

Checkout the repository:
```
git clone https://moschin@bitbucket.org/moschin/tester-matching-import.git
```

Clean and install the module using the maven command:
```
mvn clean install -U
```

## Running the tests

Use the following command to run unit tests:
```
mvn test
```

## Deployment

To deploy (run) the application, go to the project's folder and run the following command from CMD line:
```
mvn spring-boot:run
```
* Then create folder "C:\import" and copy CSV files (devices.csv, bugs.csv, testers.csv, tester_device.csv) into it.
The folder can be changed by editing property: "import.location" in the "tester-matching-import\src\main\resources\application.yml" file.

* The CRON Job is set to run every minute and process the files.

* The application will be deployed on default port 8011 (can be changed in the "server.port" property)

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Versioning

0.0.1-SNAPSHOT

## Authors

* **Marcin Miszczak**