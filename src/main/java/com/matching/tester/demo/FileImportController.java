package com.matching.tester.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/import/trigger")
public class FileImportController {

    private static final Logger LOG = LoggerFactory.getLogger(FileImportController.class);

    private final JobLauncher jobLauncher;
    private final Job importJob;

    public FileImportController(JobLauncher jobLauncher, Job importJob) {
        this.jobLauncher = jobLauncher;
        this.importJob = importJob;
    }

    /**
     * Triggered by a GET request or by a CRON job.
     */
    @GetMapping
    @Scheduled(cron = "${import.cron.expression}")
    public void importData() {
        runJob();
    }

    private void runJob() {

        try {
            // create new spring boot job
            JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
            jobLauncher.run(importJob, jobParameters);
        }
        catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            LOG.error("Import job error!", e);
        }
    }
}
