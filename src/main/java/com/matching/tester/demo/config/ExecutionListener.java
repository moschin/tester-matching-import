package com.matching.tester.demo.config;

import com.matching.tester.demo.model.DataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.util.ArrayList;

import static com.matching.tester.demo.config.JobUtils.FILES_TO_IMPORT;
import static com.matching.tester.demo.config.JobUtils.TOTAL_IMPORT_TIME;
import static com.matching.tester.demo.config.JobUtils.TRANSFER_OBJECT;

@Component
public class ExecutionListener implements JobExecutionListener {

    private static final Logger LOG = LoggerFactory.getLogger(ExecutionListener.class);

    @Override
    public void beforeJob(JobExecution jobExecution) {
        jobExecution.getExecutionContext()
            .put(FILES_TO_IMPORT, new ArrayList<Path>());
        jobExecution.getExecutionContext()
            .put(TRANSFER_OBJECT, new DataModel());
        jobExecution.getExecutionContext()
            .put(TOTAL_IMPORT_TIME, (double) 0);
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        Double totalImportTime = (Double) jobExecution.getExecutionContext()
            .get(TOTAL_IMPORT_TIME);
        LOG.info("Total import time: {}", BigDecimal.valueOf(totalImportTime)
            .setScale(4, RoundingMode.HALF_UP));
    }
}
