package com.matching.tester.demo.config;

import com.matching.tester.demo.batch.FileParser;
import com.matching.tester.demo.batch.FileProcessor;
import com.matching.tester.demo.batch.FileReceiver;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableBatchProcessing
@EnableScheduling
public class SimpleTesterMatchingJobConfig {

    private final JobBuilderFactory jobs;
    private final StepBuilderFactory steps;

    private final FileParser fileParser;
    private final FileProcessor fileProcessor;
    private final FileReceiver fileReceiver;

    public SimpleTesterMatchingJobConfig(JobBuilderFactory jobs, StepBuilderFactory steps, FileParser fileParser,
        FileProcessor fileProcessor, FileReceiver fileReceiver)
    {
        this.jobs = jobs;
        this.steps = steps;
        this.fileParser = fileParser;
        this.fileProcessor = fileProcessor;
        this.fileReceiver = fileReceiver;
    }

    @Bean
    public Job importJob(ExecutionListener executionListener) {
        return jobs.get(JobUtils.JOB_NAME)
            .listener(executionListener)
            .preventRestart()
            .incrementer(new RunIdIncrementer())
            .start(receiveFiles())
            .next(parseFiles())
            .next(processFiles())
            .build();
    }

    @Bean
    protected Step receiveFiles() {
        return steps.get(JobUtils.RECEIVE_FILES_STEP)
            .tasklet(fileReceiver)
            .build();
    }

    @Bean
    protected Step parseFiles() {
        return steps.get(JobUtils.PARSE_FILES_STEP)
            .tasklet(fileParser)
            .build();
    }

    @Bean
    protected Step processFiles() {
        return steps.get(JobUtils.PROCESS_FILES_STEP)
            .tasklet(fileProcessor)
            .build();
    }
}
