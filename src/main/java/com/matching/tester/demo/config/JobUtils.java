package com.matching.tester.demo.config;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.util.StopWatch;

public class JobUtils {

    private JobUtils() {
    }

    public static final String RECEIVE_FILES_STEP = "receiveFiles";
    public static final String PARSE_FILES_STEP = "parseFiles";
    public static final String PROCESS_FILES_STEP = "processFiles";
    public static final String SAVE_DATA_STEP = "saveData";

    public static final String TOTAL_IMPORT_TIME = "totalImportTime";
    public static final String FILES_TO_IMPORT = "filesToImport";
    public static final String TRANSFER_OBJECT = "transferObject";
    public static final String JOB_NAME = "csvImport";

    public static <T> T getJobContextValue(StepExecution stepExecution, String key) {
        return (T) stepExecution.getJobExecution()
            .getExecutionContext()
            .get(key);
    }

    public static <T> void setJobContextValue(ChunkContext chunkContext, String key, T value) {
        chunkContext.getStepContext()
            .getStepExecution()
            .getJobExecution()
            .getExecutionContext()
            .put(key, value);
    }

    public static void releaseJobContextValue(ChunkContext chunkContext, String key) {
        chunkContext.getStepContext()
            .getStepExecution()
            .getJobExecution()
            .getExecutionContext()
            .remove(key);
    }

    public static void stopStepAndJob(StepContribution stepContribution, ChunkContext chunkContext) {
        stepContribution.setExitStatus(ExitStatus.FAILED);
        chunkContext.getStepContext()
            .getStepExecution()
            .getJobExecution()
            .setExitStatus(ExitStatus.FAILED);
        chunkContext.getStepContext()
            .getStepExecution()
            .getJobExecution()
            .stop();
    }

    public static void start(StopWatch stopWatch) {
        stopWatch.start();
    }

    public static Double stop(StopWatch stopWatch) {
        stopWatch.stop();
        return stopWatch.getTotalTimeSeconds();
    }
}
