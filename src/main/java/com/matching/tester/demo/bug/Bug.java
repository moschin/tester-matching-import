package com.matching.tester.demo.bug;

import com.matching.tester.demo.tester.Tester;
import com.matching.tester.demo.device.Device;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "BUG")
public class Bug {

    @Id
    @Column(name = "BUG_ID")
    private Long id;

    @ManyToOne
    private Device device;

    @ManyToOne
    private Tester tester;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Tester getTester() {
        return tester;
    }

    public void setTester(Tester tester) {
        this.tester = tester;
    }
}
