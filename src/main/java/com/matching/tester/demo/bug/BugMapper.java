package com.matching.tester.demo.bug;

import com.matching.tester.demo.bug.Bug;
import com.matching.tester.demo.model.BugData;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BugMapper {

    @Mapping(target = "device.id", source = "deviceId")
    @Mapping(target = "tester.id", source = "testerId")
    Bug toBug(BugData bugData);
}
