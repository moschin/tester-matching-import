package com.matching.tester.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleTesterMatchingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleTesterMatchingApplication.class, args);
	}

}
