package com.matching.tester.demo.device;

import com.matching.tester.demo.api.DeviceDto;
import com.matching.tester.demo.device.Device;
import com.matching.tester.demo.model.DeviceData;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DeviceMapper {

    Device toDevice(DeviceData deviceData);

    DeviceDto toDeviceDto(Device device);
}
