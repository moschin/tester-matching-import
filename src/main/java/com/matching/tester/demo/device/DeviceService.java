package com.matching.tester.demo.device;

import com.matching.tester.demo.api.DeviceDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DeviceService {

    private final DeviceRepository deviceRepository;
    private final DeviceMapper deviceMapper;

    public DeviceService(DeviceRepository deviceRepository, DeviceMapper deviceMapper) {
        this.deviceRepository = deviceRepository;
        this.deviceMapper = deviceMapper;
    }

    public List<DeviceDto> getDevices() {
        return deviceRepository.findAll()
            .stream()
            .map(deviceMapper::toDeviceDto)
            .collect(Collectors.toList());
    }
}
