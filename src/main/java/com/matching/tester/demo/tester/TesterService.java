package com.matching.tester.demo.tester;

import com.matching.tester.demo.api.CountryDto;
import com.matching.tester.demo.api.DeviceDto;
import com.matching.tester.demo.api.SearchCriteriaRequestDto;
import com.matching.tester.demo.api.SearchCriteriaResponseDto;
import com.matching.tester.demo.api.SearchResultDto;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TesterService {

    private final TesterRepository testerRepository;
    private final TesterMapper testerMapper;

    public TesterService(TesterRepository testerRepository, TesterMapper testerMapper) {
        this.testerRepository = testerRepository;
        this.testerMapper = testerMapper;
    }

    public List<CountryDto> getCountries() {
        return testerRepository.findAllCountries()
            .stream()
            .map(testerMapper::toCountryDto)
            .collect(Collectors.toList());
    }

    public SearchCriteriaResponseDto getSearchResult(SearchCriteriaRequestDto searchCriteriaRequestDto) {
        List<Long> deviceIds = searchCriteriaRequestDto.getDevices()
            .stream()
            .map(DeviceDto::getId)
            .map(Integer::longValue)
            .collect(Collectors.toList());
        List<String> countries = searchCriteriaRequestDto.getCountries()
            .stream()
            .map(CountryDto::getCountryCode)
            .collect(Collectors.toList());
        List<SearchResultData> result = testerRepository.findTestersBySearchCriteria(countries, deviceIds);
        List<SearchResultDto> resultDto = result.stream()
            .map(testerMapper::toSearchResultDto)
            .sorted(Comparator.comparingInt(SearchResultDto::getBugsCount).reversed())
            .collect(Collectors.toList());
        return new SearchCriteriaResponseDto().statistics(resultDto);
    }
}
