package com.matching.tester.demo.tester;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TesterRepository extends JpaRepository<Tester, Long>, JpaSpecificationExecutor<Tester> {

    @Query("SELECT DISTINCT t.country FROM Tester t")
    List<String> findAllCountries();

    //    SELECT b.TESTER_TESTER_ID, t.FIRST_NAME, t.LAST_NAME  , COUNT(b.BUG_ID)
    //    FROM BUG b
    //    INNER JOIN TESTER t ON t.TESTER_ID = b.TESTER_TESTER_ID
    //    INNER JOIN DEVICE d ON d.DEVICE_ID = b.DEVICE_DEVICE_ID
    //    WHERE t.COUNTRY IN ('JP')
    //    AND d.DEVICE_ID IN (5)
    //    GROUP BY b.TESTER_TESTER_ID

    @Query("SELECT new com.matching.tester.demo.tester.SearchResultData(t.firstName, t.lastName, COUNT(b)) "
        + "FROM Bug b "
        + "INNER JOIN Tester t ON t.id = b.tester.id "
        + "INNER JOIN Device d ON d.id = b.device.id "
        + "WHERE t.country IN (:countries) "
        + "AND d.id IN (:deviceIds) "
        + "GROUP BY b.tester.id")
    List<SearchResultData> findTestersBySearchCriteria(@Param("countries") Collection countries, @Param("deviceIds") Collection deviceIds);

}
