package com.matching.tester.demo.tester;

import com.matching.tester.demo.api.CountryDto;
import com.matching.tester.demo.api.SearchCriteriaRequestDto;
import com.matching.tester.demo.api.SearchCriteriaResponseDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/testers")
public class TesterController {

    private final TesterService testerService;

    public TesterController(TesterService testerService) {
        this.testerService = testerService;
    }

    @GetMapping("/countries")
    public List<CountryDto> getTestersCountries() {
        return testerService.getCountries();
    }

    @PostMapping("/statistics")
    public SearchCriteriaResponseDto getSearchResult(@RequestBody @Valid SearchCriteriaRequestDto searchCriteriaRequestDto) {
        return testerService.getSearchResult(searchCriteriaRequestDto);
    }
}
