package com.matching.tester.demo.tester;

public class SearchResultData {
    private String firstName;
    private String lastName;
    private Long bugsCount;

    public SearchResultData(String firstName, String lastName, Long bugsCount) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.bugsCount = bugsCount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getBugsCount() {
        return bugsCount;
    }

    public void setBugsCount(Long bugsCount) {
        this.bugsCount = bugsCount;
    }
}
