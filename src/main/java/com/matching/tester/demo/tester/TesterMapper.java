package com.matching.tester.demo.tester;

import com.matching.tester.demo.api.CountryDto;
import com.matching.tester.demo.api.SearchResultDto;
import com.matching.tester.demo.model.TesterData;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TesterMapper {

    Tester toTester(TesterData testerData);

    @Mapping(target = "countryCode", source = "country")
    CountryDto toCountryDto(String country);

    SearchResultDto toSearchResultDto(SearchResultData searchResultData);
}
