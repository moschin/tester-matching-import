package com.matching.tester.demo.model;

public class BugData {

    private Long id;
    private Long deviceId;
    private Long testerId;

    public BugData(Long id, Long deviceId, Long testerId) {
        this.id = id;
        this.deviceId = deviceId;
        this.testerId = testerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getTesterId() {
        return testerId;
    }

    public void setTesterId(Long testerId) {
        this.testerId = testerId;
    }
}
