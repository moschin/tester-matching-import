package com.matching.tester.demo.model;

import java.util.ArrayList;
import java.util.List;

public class DataModel {

    private List<BugData> bugData = new ArrayList<>();
    private List<DeviceData> deviceData = new ArrayList<>();
    private List<TesterData> testerData = new ArrayList<>();
    private List<TesterDeviceData> testerDeviceData = new ArrayList<>();

    public List<BugData> getBugData() {
        return bugData;
    }

    public void setBugData(List<BugData> bugData) {
        this.bugData = bugData;
    }

    public List<DeviceData> getDeviceData() {
        return deviceData;
    }

    public void setDeviceData(List<DeviceData> deviceData) {
        this.deviceData = deviceData;
    }

    public List<TesterData> getTesterData() {
        return testerData;
    }

    public void setTesterData(List<TesterData> testerData) {
        this.testerData = testerData;
    }

    public List<TesterDeviceData> getTesterDeviceData() {
        return testerDeviceData;
    }

    public void setTesterDeviceData(List<TesterDeviceData> testerDeviceData) {
        this.testerDeviceData = testerDeviceData;
    }
}
