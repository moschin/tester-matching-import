package com.matching.tester.demo.model;

public class TesterDeviceData {

    private Long testerId;
    private Long deviceId;

    public TesterDeviceData(Long testerId, Long deviceId) {
        this.testerId = testerId;
        this.deviceId = deviceId;
    }

    public Long getTesterId() {
        return testerId;
    }

    public void setTesterId(Long testerId) {
        this.testerId = testerId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }
}
