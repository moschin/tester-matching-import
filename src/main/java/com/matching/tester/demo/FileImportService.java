package com.matching.tester.demo;

import com.matching.tester.demo.bug.Bug;
import com.matching.tester.demo.device.Device;
import com.matching.tester.demo.tester.Tester;
import com.matching.tester.demo.bug.BugRepository;
import com.matching.tester.demo.device.DeviceRepository;
import com.matching.tester.demo.tester.TesterRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class FileImportService {

    private final BugRepository bugRepository;
    private final DeviceRepository deviceRepository;
    private final TesterRepository testerRepository;

    public FileImportService(BugRepository bugRepository, DeviceRepository deviceRepository,
        TesterRepository testerRepository)
    {
        this.bugRepository = bugRepository;
        this.deviceRepository = deviceRepository;
        this.testerRepository = testerRepository;
    }

    public void store(List<Tester> testers, List<Device> devices, List<Bug> bugs) {
        testerRepository.saveAll(testers);
        deviceRepository.saveAll(devices);
        bugRepository.saveAll(bugs);
    }
}
