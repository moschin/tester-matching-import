package com.matching.tester.demo.parser;

import com.matching.tester.demo.model.DataModel;
import com.matching.tester.demo.model.DeviceData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static com.matching.tester.demo.config.JobUtils.TRANSFER_OBJECT;
import static com.matching.tester.demo.config.JobUtils.getJobContextValue;

@Component
public class DeviceParser extends ImportFileHandler<DeviceData> {

    private static final Logger LOG = LoggerFactory.getLogger(DeviceParser.class);

    @Override
    public void parse(Path path, StepExecution stepExecution) throws IOException {
        List<DeviceData> deviceData = new ArrayList<>();
        super.convertToDataObject(deviceData, path);

        DataModel dataModelObject = getJobContextValue(stepExecution, TRANSFER_OBJECT);
        dataModelObject.setDeviceData(deviceData);

        LOG.info("PARSE for the DEVICES");
    }

    @Override
    protected Consumer<? super String> addToList(List<DeviceData> list) {
        return line -> {
            String cleanLine = line.replace("\"", "");
            String[] fields = cleanLine.split(",");
            Long id = Long.valueOf(fields[0]);
            String description = fields[1];
            list.add(new DeviceData(id, description));
        };
    }
}
