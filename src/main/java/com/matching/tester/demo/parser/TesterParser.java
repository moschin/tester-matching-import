package com.matching.tester.demo.parser;

import com.matching.tester.demo.model.DataModel;
import com.matching.tester.demo.model.TesterData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static com.matching.tester.demo.config.JobUtils.TRANSFER_OBJECT;
import static com.matching.tester.demo.config.JobUtils.getJobContextValue;

@Component
public class TesterParser extends ImportFileHandler<TesterData> {

    private static final Logger LOG = LoggerFactory.getLogger(TesterParser.class);

    @Override
    public void parse(Path path, StepExecution stepExecution) throws IOException {
        List<TesterData> testerData = new ArrayList<>();
        super.convertToDataObject(testerData, path);

        DataModel dataModelObject = getJobContextValue(stepExecution, TRANSFER_OBJECT);
        dataModelObject.setTesterData(testerData);

        LOG.info("PARSE for the TESTERS");
    }

    @Override
    protected Consumer<? super String> addToList(List<TesterData> list) {
        return line -> {
            String cleanLine = line.replace("\"", "");
            String[] fields = cleanLine.split(",");
            Long id = Long.valueOf(fields[0]);
            String firstName = fields[1];
            String lastName = fields[2];
            String country = fields[3];
            list.add(new TesterData(id, firstName, lastName, country));
        };
    }
}
