package com.matching.tester.demo.parser;

import com.matching.tester.demo.model.DataModel;
import com.matching.tester.demo.model.TesterDeviceData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static com.matching.tester.demo.config.JobUtils.TRANSFER_OBJECT;
import static com.matching.tester.demo.config.JobUtils.getJobContextValue;

@Component
public class TesterDeviceParser extends ImportFileHandler<TesterDeviceData> {

    private static final Logger LOG = LoggerFactory.getLogger(TesterDeviceParser.class);

    @Override
    public void parse(Path path, StepExecution stepExecution) throws IOException {
        List<TesterDeviceData> testerDeviceData = new ArrayList<>();
        super.convertToDataObject(testerDeviceData, path);

        DataModel dataModelObject = getJobContextValue(stepExecution, TRANSFER_OBJECT);
        dataModelObject.setTesterDeviceData(testerDeviceData);
        LOG.info("PARSE for the TESTERS AND DEVICES");
    }

    @Override
    protected Consumer<? super String> addToList(List<TesterDeviceData> list) {
        return line -> {
            String cleanLine = line.replace("\"", "");
            String[] fields = cleanLine.split(",");
            Long testerId = Long.valueOf(fields[0]);
            Long deviceId = Long.valueOf(fields[1]);
            list.add(new TesterDeviceData(testerId, deviceId));
        };
    }
}
