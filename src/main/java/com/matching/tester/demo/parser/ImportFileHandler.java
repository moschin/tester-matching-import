package com.matching.tester.demo.parser;

import org.springframework.batch.core.StepExecution;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Component
public abstract class ImportFileHandler<T> {

    public abstract void parse(Path path, StepExecution stepExecution) throws IOException;

    void convertToDataObject(List<T> list, Path path) throws IOException {
        try (Stream<String> stream = Files.lines(path).skip(1)) {
            stream
                .filter(Objects::nonNull)
                .filter(Predicate.not(String::isEmpty))
                .forEach(addToList(list));
        }
    }

    protected abstract Consumer<? super String> addToList(List<T> list);
}
