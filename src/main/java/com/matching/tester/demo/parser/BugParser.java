package com.matching.tester.demo.parser;

import com.matching.tester.demo.model.BugData;
import com.matching.tester.demo.model.DataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static com.matching.tester.demo.config.JobUtils.TRANSFER_OBJECT;
import static com.matching.tester.demo.config.JobUtils.getJobContextValue;

@Component
public class BugParser extends ImportFileHandler<BugData> {

    private static final Logger LOG = LoggerFactory.getLogger(BugParser.class);

    @Override
    public void parse(Path path, StepExecution stepExecution) throws IOException {
        List<BugData> bugData = new ArrayList<>();
        super.convertToDataObject(bugData, path);

        DataModel dataModelObject = getJobContextValue(stepExecution, TRANSFER_OBJECT);
        dataModelObject.setBugData(bugData);

        LOG.info("PARSE for the BUGS");
    }

    @Override
    protected Consumer<? super String> addToList(List<BugData> list) {
        return line -> {
            String cleanLine = line.replace("\"", "");
            String[] fields = cleanLine.split(",");
            Long id = Long.valueOf(fields[0]);
            Long deviceId = Long.valueOf(fields[1]);
            Long testerId = Long.valueOf(fields[2]);
            list.add(new BugData(id, deviceId, testerId));
        };
    }

}
