package com.matching.tester.demo.batch;

import com.matching.tester.demo.config.JobUtils;
import com.matching.tester.demo.parser.ImportFileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import static com.matching.tester.demo.config.JobUtils.FILES_TO_IMPORT;
import static com.matching.tester.demo.config.JobUtils.getJobContextValue;
import static com.matching.tester.demo.config.JobUtils.setJobContextValue;
import static com.matching.tester.demo.config.JobUtils.start;
import static com.matching.tester.demo.config.JobUtils.stop;
import static com.matching.tester.demo.config.JobUtils.stopStepAndJob;

@Component
public class FileParser extends StepExecutionListenerSupport implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger(FileParser.class);

    private ImportFileHandler testerParser;

    private ImportFileHandler deviceParser;

    private ImportFileHandler bugParser;

    private ImportFileHandler testerDeviceParser;

    public FileParser(ImportFileHandler testerParser, ImportFileHandler deviceParser, ImportFileHandler bugParser,
        ImportFileHandler testerDeviceParser) {
        this.testerParser = testerParser;
        this.deviceParser = deviceParser;
        this.bugParser = bugParser;
        this.testerDeviceParser = testerDeviceParser;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
        StopWatch stopWatch = new StopWatch();
        start(stopWatch);

        Double totalImportTime = getJobContextValue(stepContribution.getStepExecution(), JobUtils.TOTAL_IMPORT_TIME);
        List<Path> filesFromContext = getJobContextValue(stepContribution.getStepExecution(), FILES_TO_IMPORT);
        filesFromContext.forEach(path -> {
            try {
                parseToModel(path, stepContribution.getStepExecution());
            }
            catch (IOException e) {
                LOG.error("Error during parsing the file: {}" + path.getFileName().toString(), e);
                stopStepAndJob(stepContribution, chunkContext);
            }
        });

        Double parsingTime = stop(stopWatch);
        totalImportTime += parsingTime;
        setJobContextValue(chunkContext, JobUtils.TOTAL_IMPORT_TIME, totalImportTime);
        LOG.info("Total files parsing time {} sec.", parsingTime);

        return RepeatStatus.FINISHED;
    }

    private void parseToModel(Path path, StepExecution stepExecution) throws IOException {
        String fileName = path.getFileName().toString();
        switch (fileName) {
            case "testers.csv":
                testerParser.parse(path, stepExecution);
                break;
            case "devices.csv":
                deviceParser.parse(path, stepExecution);
                break;
            case "bugs.csv":
                bugParser.parse(path, stepExecution);
                break;
            case "tester_device.csv":
                testerDeviceParser.parse(path, stepExecution);
                break;
            default:
                break;
        }
    }

}
