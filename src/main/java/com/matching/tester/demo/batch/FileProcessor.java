package com.matching.tester.demo.batch;

import com.matching.tester.demo.bug.Bug;
import com.matching.tester.demo.config.JobUtils;
import com.matching.tester.demo.device.Device;
import com.matching.tester.demo.tester.Tester;
import com.matching.tester.demo.bug.BugMapper;
import com.matching.tester.demo.device.DeviceMapper;
import com.matching.tester.demo.tester.TesterMapper;
import com.matching.tester.demo.model.DataModel;
import com.matching.tester.demo.model.TesterDeviceData;
import com.matching.tester.demo.FileImportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static com.matching.tester.demo.config.JobUtils.TRANSFER_OBJECT;
import static com.matching.tester.demo.config.JobUtils.getJobContextValue;
import static com.matching.tester.demo.config.JobUtils.setJobContextValue;
import static com.matching.tester.demo.config.JobUtils.start;
import static com.matching.tester.demo.config.JobUtils.stop;

@Component
public class FileProcessor extends StepExecutionListenerSupport implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger(FileProcessor.class);

    private final TesterMapper testerMapper;

    private final DeviceMapper deviceMapper;

    private final BugMapper bugMapper;

    private final FileImportService fileImportService;

    public FileProcessor(TesterMapper testerMapper, DeviceMapper deviceMapper, BugMapper bugMapper,
        FileImportService fileImportService)
    {
        this.testerMapper = testerMapper;
        this.deviceMapper = deviceMapper;
        this.bugMapper = bugMapper;
        this.fileImportService = fileImportService;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
        StopWatch stopWatch = new StopWatch();
        start(stopWatch);

        Double totalImportTime = getJobContextValue(stepContribution.getStepExecution(), JobUtils.TOTAL_IMPORT_TIME);
        DataModel dataModelObject = getJobContextValue(stepContribution.getStepExecution(), TRANSFER_OBJECT);

        List<Tester> testers = dataModelObject.getTesterData()
            .stream()
            .map(testerMapper::toTester)
            .collect(Collectors.toList());

        List<Device> devices = dataModelObject.getDeviceData()
            .stream()
            .map(deviceMapper::toDevice)
            .collect(Collectors.toList());

        List<Bug> bugs = dataModelObject.getBugData()
            .stream()
            .map(bugMapper::toBug)
            .collect(Collectors.toList());

        testers
            .forEach(tester -> {
                List<TesterDeviceData> dataModels = dataModelObject.getTesterDeviceData()
                    .stream()
                    .filter(data -> tester.getId()
                        .equals(data.getTesterId()))
                    .collect(Collectors.toList());

                List<Device> filteredDevices = devices.stream()
                    .filter(device -> dataModels.stream()
                        .anyMatch(data -> device.getId()
                            .equals(data.getDeviceId())))
                    .collect(Collectors.toList());

                tester.setDevices(new HashSet<>(filteredDevices));
            });

        fileImportService.store(testers, devices, bugs);

        Double processingTime = stop(stopWatch);
        totalImportTime += processingTime;
        setJobContextValue(chunkContext, JobUtils.TOTAL_IMPORT_TIME, totalImportTime);
        LOG.info("Total files processing time {} sec.", processingTime);

        return RepeatStatus.FINISHED;
    }
}
