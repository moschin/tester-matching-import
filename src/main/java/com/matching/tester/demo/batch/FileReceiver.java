package com.matching.tester.demo.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.matching.tester.demo.config.JobUtils.FILES_TO_IMPORT;
import static com.matching.tester.demo.config.JobUtils.TOTAL_IMPORT_TIME;
import static com.matching.tester.demo.config.JobUtils.getJobContextValue;
import static com.matching.tester.demo.config.JobUtils.setJobContextValue;
import static com.matching.tester.demo.config.JobUtils.start;
import static com.matching.tester.demo.config.JobUtils.stop;
import static com.matching.tester.demo.config.JobUtils.stopStepAndJob;

@Component
public class FileReceiver extends StepExecutionListenerSupport implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger(FileReceiver.class);

    @Value("${import.location}")
    private String importLocation;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
        StopWatch stopWatch = new StopWatch();
        start(stopWatch);

        List<Path> filesFromContext = getJobContextValue(stepContribution.getStepExecution(), FILES_TO_IMPORT);
        Optional<List<Path>> filesToImport = this.getFilesToImport();

        if (filesToImport.isEmpty() || filesToImport.get()
            .isEmpty())
        {
            stopStepAndJob(stepContribution, chunkContext);
            return RepeatStatus.FINISHED;
        }

        String importFiles = filesToImport.get()
            .stream()
            .map(p -> p.getFileName()
                .toString())
            .collect(Collectors.joining(","));

        LOG.info("Files to import: {}.", importFiles);
        filesFromContext.addAll(filesToImport.get());

        Double receivingTime = stop(stopWatch);
        setJobContextValue(chunkContext, TOTAL_IMPORT_TIME, receivingTime);

        LOG.info("Total files receiving time {} sec.", receivingTime);
        return RepeatStatus.FINISHED;
    }

    private Optional<List<Path>> getFilesToImport() {
        List<Path> result = null;
        try (Stream<Path> walk = Files.walk(Paths.get(importLocation))) {
            result = walk.filter(Files::isRegularFile)
                .filter(path -> path.toString()
                    .endsWith(".csv"))
                .collect(Collectors.toList());
        }
        catch (IOException e) {
            LOG.error("Error occurred during inbox location reading.", e);
        }
        return Optional.ofNullable(result);
    }
}
