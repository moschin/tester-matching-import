package com.matching.tester.demo.tester;

import com.matching.tester.demo.api.CountryDto;
import com.matching.tester.demo.api.DeviceDto;
import com.matching.tester.demo.api.SearchCriteriaRequestDto;
import com.matching.tester.demo.api.SearchCriteriaResponseDto;
import com.matching.tester.demo.api.SearchResultDto;
import com.matching.tester.demo.device.Device;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class TesterServiceTest {

    @Mock
    private TesterRepository testerRepository;

    @InjectMocks
    private TesterService testerService;

    @Spy
    private TesterMapper testerMapper = new TesterMapperImpl();

    @Test
    void getCountries() {
        // given
        given(testerRepository.findAllCountries()).willReturn(List.of("US", "PL"));

        // when
        List<CountryDto> countries = testerService.getCountries();

        // then
        assertThat(countries).isNotNull();
        assertThat(countries).isNotEmpty();
        assertThat(countries).hasSize(2);
        assertThat(countries.get(0)).extracting(CountryDto::getCountryCode)
            .isEqualTo("US");
    }

    @Test
    void getTestersWithBugsBySearchCriteria() {
        // given
        SearchResultData searchResultData1 = new SearchResultData("John", "Doe", 10L);
        SearchResultData searchResultData2 = new SearchResultData("Mark", "Spencer", 5L);
        SearchResultData searchResultData3 = new SearchResultData("Steven", "Woo", 23L);

        SearchCriteriaRequestDto request = new SearchCriteriaRequestDto();
        DeviceDto deviceDto = new DeviceDto();
        deviceDto.setId(1);
        deviceDto.setDescription("Nexus");

        CountryDto countryDto = new CountryDto();
        countryDto.setCountryCode("PL");

        request.setDevices(List.of(deviceDto));
        request.setCountries(List.of(countryDto));

        given(testerRepository.findTestersBySearchCriteria(any(), any())).willReturn(
            List.of(searchResultData1, searchResultData2, searchResultData3));

        // when
        SearchCriteriaResponseDto searchResult = testerService.getSearchResult(request);

        // then
        assertThat(searchResult).isNotNull();
        assertThat(searchResult.getStatistics()).isNotNull();
        assertThat(searchResult.getStatistics()).isNotEmpty();
        assertThat(searchResult.getStatistics()).hasSize(3);
        assertThat(searchResult.getStatistics().get(0)).extracting(SearchResultDto::getBugsCount).isEqualTo(23);
        assertThat(searchResult.getStatistics().get(1)).extracting(SearchResultDto::getBugsCount).isEqualTo(10);
        assertThat(searchResult.getStatistics().get(2)).extracting(SearchResultDto::getBugsCount).isEqualTo(5);
    }
}
