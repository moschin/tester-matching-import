package com.matching.tester.demo.tester;

import com.matching.tester.demo.bug.Bug;
import com.matching.tester.demo.device.Device;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TesterRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private TesterRepository testerRepository;

    @Before
    public void setUp(){
        // given
        Tester tester1 = new Tester();
        tester1.setId(1L);
        tester1.setFirstName("John");
        tester1.setLastName("Doe");
        tester1.setCountry("US");

        Tester tester2 = new Tester();
        tester2.setId(2L);
        tester2.setLastName("Steven");
        tester2.setLastName("Green");
        tester2.setCountry("US");

        Device device1 = new Device();
        device1.setId(1L);
        device1.setDescription("LG G6");

        Device device2 = new Device();
        device2.setId(2L);
        device2.setDescription("Samsung Galaxy 10");

        tester1.setDevices(Set.of(device1));
        tester2.setDevices(Set.of(device2));

        Bug bug1 = new Bug();
        bug1.setId(1L);
        bug1.setTester(tester1);
        bug1.setDevice(device1);

        Bug bug2 = new Bug();
        bug2.setId(2L);
        bug2.setTester(tester1);
        bug2.setDevice(device1);

        Bug bug3 = new Bug();
        bug3.setId(3L);
        bug3.setTester(tester2);
        bug3.setDevice(device2);

        testEntityManager.persist(tester1);
        testEntityManager.persist(tester2);
        testEntityManager.persist(bug1);
        testEntityManager.persist(bug2);
        testEntityManager.persist(bug3);
    }

    @Test
    public void whenFindByCriteria_thenReturnSearchResultData() {
        // when
        List<SearchResultData> searchResult =
            testerRepository.findTestersBySearchCriteria(Arrays.asList("US"), Arrays.asList(1L));

        // then
        assertThat(searchResult).isNotNull();
        assertThat(searchResult).isNotEmpty();
        assertThat(searchResult).hasSize(1);
        assertThat(searchResult).extracting(SearchResultData::getBugsCount).isEqualTo(List.of(2L));
        assertThat(searchResult).extracting(SearchResultData::getFirstName).isEqualTo(List.of("John"));
        assertThat(searchResult).extracting(SearchResultData::getLastName).isEqualTo(List.of("Doe"));
    }
}
