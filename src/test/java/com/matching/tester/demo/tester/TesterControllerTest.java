package com.matching.tester.demo.tester;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.matching.tester.demo.api.CountryDto;
import com.matching.tester.demo.api.SearchCriteriaRequestDto;
import com.matching.tester.demo.api.SearchCriteriaResponseDto;
import com.matching.tester.demo.api.SearchResultDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TesterController.class)
public class TesterControllerTest {

    private static final String BASE_URL = "/testers";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TesterService testerService;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void getCountries() throws Exception {
        // given
        CountryDto country1 = new CountryDto();
        country1.setCountryCode("PL");

        CountryDto country2 = new CountryDto();
        country2.setCountryCode("EN");

        List<CountryDto> responseDto = List.of(country1, country2);

        given(testerService.getCountries()).willReturn(responseDto);

        // when
        ResultActions resp = mvc.perform(get(BASE_URL + "/countries").contentType(MediaType.APPLICATION_JSON));

        // then
        resp.andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[0].countryCode", equalTo("PL")))
            .andExpect(jsonPath("$.[1].countryCode", equalTo("EN")));
    }

    @Test
    public void getSearchResult() throws Exception {
        // given

        SearchCriteriaRequestDto requestDto = new SearchCriteriaRequestDto();
        SearchCriteriaResponseDto responseDto = new SearchCriteriaResponseDto();

        SearchResultDto result1 = new SearchResultDto();
        result1.setFirstName("John");
        result1.setLastName("Doe");
        result1.setBugsCount(10);

        SearchResultDto result2 = new SearchResultDto();
        result2.setFirstName("Maria");
        result2.setLastName("Zen");
        result2.setBugsCount(20);

        SearchResultDto result3 = new SearchResultDto();
        result3.setFirstName("Carla");
        result3.setLastName("Boo");
        result3.setBugsCount(5);

        responseDto.statistics(List.of(result2, result1, result3));
        given(testerService.getSearchResult(requestDto)).willReturn(responseDto);

        // when
        ResultActions resp = mvc.perform(post(BASE_URL + "/statistics").contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(requestDto)));

        // then
        resp.andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.statistics[0].bugsCount", equalTo(20)))
            .andExpect(jsonPath("$.statistics[0].firstName", equalTo("Maria")))
            .andExpect(jsonPath("$.statistics[1].bugsCount", equalTo(10)))
            .andExpect(jsonPath("$.statistics[1].firstName", equalTo("John")))
            .andExpect(jsonPath("$.statistics[2].bugsCount", equalTo(5)))
            .andExpect(jsonPath("$.statistics[2].firstName", equalTo("Carla")));
    }
}
