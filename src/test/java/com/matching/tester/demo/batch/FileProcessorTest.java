package com.matching.tester.demo.batch;

import com.matching.tester.demo.FileImportService;
import com.matching.tester.demo.bug.BugMapper;
import com.matching.tester.demo.bug.BugMapperImpl;
import com.matching.tester.demo.device.DeviceMapper;
import com.matching.tester.demo.device.DeviceMapperImpl;
import com.matching.tester.demo.model.BugData;
import com.matching.tester.demo.model.DataModel;
import com.matching.tester.demo.model.DeviceData;
import com.matching.tester.demo.model.TesterData;
import com.matching.tester.demo.model.TesterDeviceData;
import com.matching.tester.demo.tester.TesterMapper;
import com.matching.tester.demo.tester.TesterMapperImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.batch.repeat.RepeatStatus;

import static com.matching.tester.demo.config.JobUtils.TRANSFER_OBJECT;
import static com.matching.tester.demo.config.JobUtils.getJobContextValue;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class FileProcessorTest extends AbstractStepTest {

    @InjectMocks
    private FileProcessor fileProcessor;

    @Spy
    private TesterMapper testerMapper = new TesterMapperImpl();

    @Spy
    private DeviceMapper deviceMapper = new DeviceMapperImpl();

    @Spy
    private BugMapper bugMapper = new BugMapperImpl();

    @Mock
    private FileImportService fileImportService;

    @BeforeEach
    void init() {
        super.init();
    }

    @Test
    void processData() {
        // given
        DataModel dataModelObject = getJobContextValue(stepContribution.getStepExecution(), TRANSFER_OBJECT);
        BugData bugData = new BugData(1L, 2L, 3L);
        dataModelObject.getBugData().add(bugData);
        TesterData testerData = new TesterData(3L, "John", "Doe", "US");
        dataModelObject.getTesterData().add(testerData);
        DeviceData deviceData = new DeviceData(2L, "LG G6");
        dataModelObject.getDeviceData().add(deviceData);
        TesterDeviceData testerDeviceData = new TesterDeviceData(3L, 2L);
        dataModelObject.getTesterDeviceData().add(testerDeviceData);

        // when
        RepeatStatus status = fileProcessor.execute(stepContribution, chunkContext);

        // then
        assertThat(status).isEqualTo(RepeatStatus.FINISHED);
        verify(testerMapper, atLeastOnce()).toTester(testerData);
        verify(deviceMapper, atLeastOnce()).toDevice(deviceData);
        verify(bugMapper, atLeastOnce()).toBug(bugData);
        verify(fileImportService, atLeastOnce()).store(any(), any(), any());
    }
}
