package com.matching.tester.demo.batch;

import com.matching.tester.demo.config.ExecutionListener;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.batch.test.MetaDataInstanceFactory;

import static org.mockito.BDDMockito.given;

public abstract class AbstractStepTest {

    protected static final String SRC_TEST_RESOURCES = "src/test/resources/";

    @Mock
    StepExecution stepExecution;
    @Mock
    StepContext stepContext;
    @Mock
    StepContribution stepContribution;
    @Mock
    ChunkContext chunkContext;

    JobExecution jobExecution = MetaDataInstanceFactory.createJobExecution();

    @BeforeEach
    void init() {
        ExecutionListener executionListener = new ExecutionListener();
        executionListener.beforeJob(jobExecution);

        given(chunkContext.getStepContext()).willReturn(stepContext);
        given(stepContext.getStepExecution()).willReturn(stepExecution);
        given(stepExecution.getJobExecution()).willReturn(jobExecution);
        given(stepContribution.getStepExecution()).willReturn(stepExecution);

    }
}
