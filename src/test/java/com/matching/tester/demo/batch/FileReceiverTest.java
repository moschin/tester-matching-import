package com.matching.tester.demo.batch;

import com.matching.tester.demo.config.JobUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.test.util.ReflectionTestUtils;

import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class FileReceiverTest extends AbstractStepTest {

    @Spy
    private FileReceiver fileReceiver;

    @BeforeEach
    void init() {
        super.init();
    }

    @Test
    void whenFilesToImportExist() {
        // given
        ReflectionTestUtils.setField(fileReceiver, "importLocation", SRC_TEST_RESOURCES);

        // when
        RepeatStatus status = fileReceiver.execute(stepContribution, chunkContext);
        List<Path> filesToImport = JobUtils.getJobContextValue(stepContribution.getStepExecution(), JobUtils.FILES_TO_IMPORT);

        // then
        assertThat(status).isEqualTo(RepeatStatus.FINISHED);
        assertThat(filesToImport).isNotNull();
        assertThat(filesToImport).isNotEmpty();
        assertThat(filesToImport).hasSize(4);
    }

    @Test
    void whenFilesToImportNotExist() {
        // given
        ReflectionTestUtils.setField(fileReceiver, "importLocation", SRC_TEST_RESOURCES + "dummy");

        // when
        RepeatStatus repeatStatus = fileReceiver.execute(stepContribution, chunkContext);
        List<Path> filesToImport = JobUtils.getJobContextValue(stepContribution.getStepExecution(), JobUtils.FILES_TO_IMPORT);

        // then
        assertThat(repeatStatus).isEqualTo(RepeatStatus.FINISHED);
        assertThat(filesToImport).isNotNull();
        assertThat(filesToImport).isEmpty();
        assertThat(jobExecution.getExitStatus()).isEqualTo(ExitStatus.FAILED);
    }
}
