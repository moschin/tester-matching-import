package com.matching.tester.demo.batch;

import com.matching.tester.demo.config.JobUtils;
import com.matching.tester.demo.parser.BugParser;
import com.matching.tester.demo.parser.DeviceParser;
import com.matching.tester.demo.parser.ImportFileHandler;
import com.matching.tester.demo.parser.TesterDeviceParser;
import com.matching.tester.demo.parser.TesterParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class FileParserTest extends AbstractStepTest {

    private FileParser fileParser;

    @Spy
    private ImportFileHandler testerParser = new TesterParser();

    @Spy
    private ImportFileHandler deviceParser = new DeviceParser();

    @Spy
    private ImportFileHandler bugParser = new BugParser();

    @Spy
    private ImportFileHandler testerDeviceParser = new TesterDeviceParser();

    @BeforeEach
    void init() {
        super.init();
    }

    @Test
    void parseCsvFiles() throws IOException {
        // given
        fileParser = new FileParser(testerParser, deviceParser, bugParser, testerDeviceParser);
        List<Path> filesToImport = JobUtils.getJobContextValue(stepContribution.getStepExecution(), JobUtils.FILES_TO_IMPORT);
        Path testersPath = Path.of(SRC_TEST_RESOURCES + "testers.csv");
        Path devicesPath = Path.of(SRC_TEST_RESOURCES + "devices.csv");
        Path bugsPath = Path.of(SRC_TEST_RESOURCES + "bugs.csv");
        Path testerDevicePath = Path.of(SRC_TEST_RESOURCES + "tester_device.csv");
        filesToImport.addAll(Arrays.asList(testersPath, devicesPath, bugsPath, testerDevicePath));

        // when
        RepeatStatus status = fileParser.execute(stepContribution, chunkContext);

        // then
        assertThat(status).isEqualTo(RepeatStatus.FINISHED);
        verify(testerParser, atLeastOnce()).parse(eq(testersPath), any());
        verify(deviceParser, atLeastOnce()).parse(eq(devicesPath), any());
        verify(bugParser, atLeastOnce()).parse(eq(bugsPath), any());
        verify(testerDeviceParser, atLeastOnce()).parse(eq(testerDevicePath), any());
    }
}
