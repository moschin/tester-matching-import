package com.matching.tester.demo.device;

import com.matching.tester.demo.api.DeviceDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class DeviceServiceTest {

    @Mock
    private DeviceRepository deviceRepository;

    @InjectMocks
    private DeviceService deviceService;

    @Spy
    private DeviceMapper deviceMapper = new DeviceMapperImpl();

    @Test
    void getDevices() {
        // given
        Device device = new Device();
        device.setId(1L);
        device.setDescription("LG G6");

        given(deviceRepository.findAll()).willReturn(Arrays.asList(device));

        // when
        List<DeviceDto> devicesDto = deviceService.getDevices();

        // then
        assertThat(devicesDto).isNotNull();
        assertThat(devicesDto).isNotEmpty();
        assertThat(devicesDto).hasSize(1);
        assertThat(devicesDto.get(0)).extracting(DeviceDto::getDescription).isEqualTo("LG G6");
    }
}
