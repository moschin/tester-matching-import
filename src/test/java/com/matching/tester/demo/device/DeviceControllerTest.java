package com.matching.tester.demo.device;

import com.matching.tester.demo.api.DeviceDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(DeviceController.class)
public class DeviceControllerTest {

    private static final String URL = "/devices";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DeviceService deviceService;

    @Test
    public void getDevices() throws Exception {
        // given
        DeviceDto device1 = new DeviceDto();
        device1.setId(1);
        device1.setDescription("Xiaomi");

        DeviceDto device2 = new DeviceDto();
        device2.setId(1);
        device2.setDescription("LG");

        List<DeviceDto> responseDto = new ArrayList<>();
        responseDto.add(device1);
        responseDto.add(device2);

        given(deviceService.getDevices()).willReturn(responseDto);

        // when
        ResultActions resp = mvc.perform(get(URL).contentType(MediaType.APPLICATION_JSON));

        // then
        resp.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.[0].description", equalTo("Xiaomi")))
            .andExpect(jsonPath("$.[1].description", equalTo("LG")));
    }
}
